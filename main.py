from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from databases import Database
from pydantic import BaseModel
from prometheus_fastapi_instrumentator import Instrumentator
import httpx


async def fetch_last_item_from_external_api():
    async with httpx.AsyncClient() as client:
        response = await client.get('URL_API_EXTERNE')
        return response.json()

class DynamicItem(BaseModel):
    data: dict

DATABASE_URL = "postgresql://postgres:monSuperMotDePasse@mon-postgres/apicloud"

engine = create_engine(DATABASE_URL)
metadata = MetaData()

items = Table(
    "dynamic_items",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("data", JSON)
)

Base = declarative_base()

database = Database(DATABASE_URL)

app = FastAPI()

@app.on_event("startup")
async def startup():
    await database.connect()
    metadata.create_all(bind=engine)

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

class Item(BaseModel):
    id: int
    data: str

@app.post("/items/")
async def create_dynamic_item(item: DynamicItem):
    query = items.insert().values(data=item.data)
    last_record_id = await database.execute(query)
    return {"id": last_record_id, "data": item.data}

@app.post("/dynamic-items/")
async def create_dynamic_item(item: DynamicItem):
    #voir avec eux le type de bdd
    initial_query = items.insert().values(data=item)
    await database.execute(initial_query)
    
    external_item_data = await fetch_last_item_from_external_api()
    
    external_query = items.insert().values(data=external_item_data)
    last_record_id = await database.execute(external_query)
    
    return {"id": last_record_id, "data": external_item_data}

@app.get("/item/{item_id}")
async def read_items(item_id: int):
    query = items.select().where(items.c.id==item_id)
    return await database.fetch_all(query)

@app.get("/items/")
async def read_items():
    query = items.select()
    return await database.fetch_all(query)

@app.get("/items/{item_page}")
async def read_items(item_page:int, limit: int = 10):
    query = items.select().offset(item_page*limit).limit(limit)
    return await database.fetch_all(query)

Instrumentator().instrument(app).expose(app)